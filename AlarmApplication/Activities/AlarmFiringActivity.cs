using System;
using System.Net.Mime;
using Android.App;
using Android.Graphics;
using Android.Media;
using Android.OS;
using Android.Views;
using Android.Widget;
using Java.IO;
using Java.Net;

namespace AlarmApplication.Activities
{
    /// <summary>
    /// ��������, ���������� �� �� ��� ����� ���������� ��� ������������ ����������.
    /// </summary>
	[Activity (Label = "AlarmFiringActivity")]			
	public class AlarmFiringActivity : Activity
	{
        /// <summary>
        /// ��������
        /// </summary>
	    private Vibrator _vibrator;

        /// <summary>
        /// �������
        /// </summary>
	    private Ringtone _ringtone;

        private const string AlarmIdExtra = "alarmId";

        private int _alarmId;

        private Alarm _alarm;

        /// <summary>
        /// ��� ��������. ����������� ���. ��������� �������� � �������.
        /// </summary>
        /// <param name="bundle">Bundle.</param>
		protected override void OnCreate (Bundle bundle)
		{
            //������ �� ���� ��� ���������.
			RequestWindowFeature (WindowFeatures.NoTitle);
			base.OnCreate (bundle);

            //����������� ���.
			SetContentView(Resource.Layout.AlarmFiringLayout);

            //�������� ������� ����������.
			var okButton = FindViewById<Button> (Resource.Id.AlarmFiringOkButton);
            var snoozeButton = FindViewById<Button>(Resource.Id.AlarmFiringSnoozeButton);
            var alarmFiringLabel = FindViewById<TextView>(Resource.Id.AlarmFiringLabel);
            var alarmFiringImage = FindViewById<ImageView>(Resource.Id.AlarmFiringImage);

            if (Intent.HasExtra(AlarmIdExtra))
            {
                _alarmId = Intent.Extras.GetInt(AlarmIdExtra);
            }
            _alarm = AlarmList.Alarms[_alarmId];
            alarmFiringLabel.Text = _alarm.Label;

            Display display = WindowManager.DefaultDisplay;
            var size = display.Width < display.Height
                                    ? display.Width
                                    : display.Height;

            using (var image = ImageResizer.DecodeSampledBitmapFromFile(_alarm.ImageFileUri, size, size))
            {
                alarmFiringImage.SetImageBitmap(image);
            }

            //alarmFiringImage.SetImageURI(Android.Net.Uri.Parse(_alarm.ImageUri));

            if (_alarm.IsVibroOn)
            {
                //�������� �����.
                _vibrator = (Vibrator)GetSystemService(VibratorService);
                long[] pattern = { 1000, 200, 200, 200 };
                _vibrator.Vibrate(pattern, 0);
            }

            //�������� �������.
            _ringtone = RingtoneManager.GetRingtone(this, Android.Net.Uri.Parse(_alarm.SoundFileUri));
            if (_ringtone!=null)
                _ringtone.Play();

             
            //okButton.Text = _alarmId.ToString();

            //������������� �� �������.
            //������ ��
			okButton.Click += delegate
			{
			    TurnOffVibroAndRingtone();
				Finish();
			};
            //������ �������
            snoozeButton.Click += (sender, e) => Snooze();
		}

        
        /// <summary>
        /// ��������� ����� � �������.
        /// </summary>
        private void TurnOffVibroAndRingtone()
        {
            if (_vibrator!=null)
            {
                _vibrator.Cancel();
            }
            if (_ringtone != null)
            {
                if (_ringtone.IsPlaying)
                    _ringtone.Stop();
                _ringtone.Dispose();
            }
            StaticWakeLock.LockOff(this);

        }

        /// <summary>
        /// ������������� ����� � ����. ������������� ��������� ����� ���� �����.
        /// </summary>
        private void Snooze()
        {

            
            var alarmManagerBroadcastReceiver = new AlarmManagerBroadcastReceiver();

            //�������� ������� ����� � ���������� ���� �����.
            var dateTimeNowPlusFiveMinutes = DateTime.Now.AddMinutes(5);
            alarmManagerBroadcastReceiver.SetSnoozeAlarm(this, dateTimeNowPlusFiveMinutes, _alarmId);
            TurnOffVibroAndRingtone();
            Finish();
        }
	}
}

