using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Text.Format;
using Android.Views;
using Android.Widget;

namespace AlarmApplication.Activities
{
	[Activity (Label = "Alarm", Icon = "@drawable/AlarmIcon")]			
	public class AlarmListActivity : ListActivity
	{
	    /// <summary>
		/// Список  будильников
		/// </summary>
		private JavaList<IDictionary<string, object>> _alarmList; //List не работает хбз почему.

	    /// <summary>
	    /// Ключ для спикска будильников. Время.
	    /// </summary>
	    private const string Time = "Time";

	    /// <summary>
	    /// Ключ для спикска будильников. Описание.
	    /// </summary>
	    private const string DaysOfWeek = "Description";

	    /// <summary>
	    /// Ключ для спикска будильников. Включен ли будильник.
	    /// </summary>
	    private const string Enabled = "Enabled";

	    protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);
			//RequestWindowFeature (WindowFeatures.NoTitle);
			//Set our view from the "main" layout resource
			SetContentView(Resource.Layout.AlarmListLayout);

            var addButton = FindViewById<Button>(Resource.Id.addButton);

			addButton.Click += (sender, e) => OnAddButton();

			LoadAlarmList ();


		}

		protected override void OnListItemClick(ListView listView, View view, int position, long id)
		{
			var intent = new Intent (this, typeof(AlarmSettingsActivity));
			intent.PutExtra("Position",position);
			StartActivity (intent);
		}

        

		private void OnAddButton()
		{
			var intent = new Intent (this, typeof(AlarmSettingsActivity));
			StartActivity (intent);		

		}


	    /// <summary>
	    /// Формирует список будильников.
	    /// </summary>
	    private void LoadAlarmList()
	    {
            // создаем массив списков
	        _alarmList = new JavaList<IDictionary<string, object>>();
	        foreach (var alarm in AlarmList.Alarms)
	        {

	            var dictionary = new JavaDictionary<string, object>(); //Dictionary не работает хбз почему.

	            //Узнаем формат времени (24 часовой или 12 часовой).
	            var timePattern = DateFormat.Is24HourFormat(Application.Context) ? "HH:mm" : "hh:mm tt";
	            //Добавляем время.
	            dictionary.Add(Time, alarm.DateTime.ToString(timePattern));

	            //Добавляем включенные дни недели
	            dictionary.Add(DaysOfWeek, alarm.GetDaysOfWeekString());
	            dictionary.Add(Enabled, alarm.IsEnabled);

	            _alarmList.Add(dictionary);
	        }

	        var from = new[] {Time, DaysOfWeek, Enabled};
	        var to = new[] {Resource.Id.ListItemTime, Resource.Id.ListItemDaysOfWeek, Resource.Id.ListItemIsEnabled};
            ListAdapter = new AlarmListAdapter(this, _alarmList, Resource.Layout.ListItem, from, to);
	    }

	    protected override void OnResume()
		{
			base.OnResume(); 

			LoadAlarmList ();
		}

        /// <summary>
        /// При выходе из активити
        /// </summary>
        protected override void OnStop()
        {
            base.OnStop();
            AlarmList.SaveAlarmListInFile();
        }

	}
}

