using System;
using Android.App;
using Android.Content;
using Android.Database;
using Android.Media;
using Android.OS;
using Android.Text.Format;
using Android.Widget;

namespace AlarmApplication.Activities
{
	[Activity (Label = "AlarmSettings", Icon = "@drawable/AlarmIcon")]			
	public class AlarmSettingsActivity : Activity
	{
        /// <summary>
        /// Номер диалога.
        /// </summary>
	    const int TimeDialogId = 0;

        /// <summary>
        /// Номер выбора картинки
        /// </summary>
        public static readonly int PickImageId = 1000;

        /// <summary>
        /// Номер выбора звука
        /// </summary>
        public static readonly int PickSoundId = 100;

        /// <summary>
        /// Будильник.
        /// </summary>
	    private Alarm _alarm;

	    /// <summary>
	    /// Номер будильника.
	    /// </summary>
	    private int _alarmId = -1;

        TextView _alarmSettingsTime;
        TextView _alarmSettingsDaysOfWeek;
        TextView _alarmSettingsImage;
        EditText _alarmSettingsLabel;
        TextView _alarmSettingsSound;
        Switch _alarmSettingsVibrate;
        Switch _alarmSettingsTimeIsMoney;
        Button _saveButton;
        Button _cancelButton;


		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			//Подкидываем View.
			SetContentView (Resource.Layout.AlarmSettingsLayout);

            if (Intent.HasExtra("Position"))
            {
                _alarmId = Intent.Extras.GetInt("Position");
                _alarm = AlarmList.Alarms[_alarmId];
            }
            else
            {
                _alarm = new Alarm();
            }


            //Находим контроллы.
             _alarmSettingsTime = FindViewById<TextView>(Resource.Id.AlarmSettingsTime);
             _alarmSettingsDaysOfWeek = FindViewById<TextView>(Resource.Id.AlarmSettingsDaysOfWeek);
             _alarmSettingsImage = FindViewById<TextView>(Resource.Id.AlarmSettingsImage);
             _alarmSettingsLabel = FindViewById<EditText>(Resource.Id.AlarmSettingsLabel);
             _alarmSettingsSound = FindViewById<TextView>(Resource.Id.AlarmSettingsSound);
             _alarmSettingsVibrate = FindViewById<Switch>(Resource.Id.AlarmSettingsVibrate);
             _alarmSettingsTimeIsMoney = FindViewById<Switch>(Resource.Id.AlarmSettingsTimeIsMoney);
             _saveButton = FindViewById<Button>(Resource.Id.AlarmSettingsSaveButton);
             _cancelButton = FindViewById<Button>(Resource.Id.AlarmSettingsCancelButton);

            //Обновляем содержимое.
             RefreshControlsContent();

            //Подписываемся на события
            _saveButton.Click += (sender, e) => SaveAlarm();
            _cancelButton.Click += (sender, e) => Finish();
            _alarmSettingsTime.Click += (sender, e) => ShowDialog(TimeDialogId);
		    _alarmSettingsImage.Click += (sender, args) =>
		    {
		        Intent = new Intent();
		        Intent.SetType("image/*");
		        Intent.SetAction(Intent.ActionGetContent);
		        StartActivityForResult(Intent.CreateChooser(Intent, "Select Picture"), PickImageId);
		    };
            _alarmSettingsSound.Click += (sender, args) =>
            {
                Intent = new Intent();
                Intent.SetType("audio/*");
                Intent.SetAction(Intent.ActionGetContent);
                StartActivityForResult(Intent.CreateChooser(Intent, "Select Audio"), PickSoundId);
            };
		}


        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            if ((resultCode == Result.Ok) && (data != null))
            {
                //Получаем URI
                if (requestCode == PickImageId)
                {
                    _alarm.ImageFileUri = data.Data.ToString();
                    _alarmSettingsImage.Text = GetRealPathToFile(data.Data);
                }
                else
                {
                    _alarm.SoundFileUri = data.Data.ToString();
                    _alarmSettingsSound.Text = GetRealPathToFile(data.Data);
                }
                GetControlsContent();
            }
        }

        private string GetRealPathToFile(Android.Net.Uri uri)
        {
            string path = null;
            // The projection contains the columns we want to return in our query.
            string[] projection = new[] { Android.Provider.MediaStore.Images.Media.InterfaceConsts.Data };
            using (ICursor cursor = ManagedQuery(uri, projection, null, null, null))
            {
                if (cursor != null)
                {
                    int columnIndex = cursor.GetColumnIndexOrThrow(Android.Provider.MediaStore.Images.Media.InterfaceConsts.Data);
                    cursor.MoveToFirst();
                    path = cursor.GetString(columnIndex);
                }
            }
            return path;
        }

        /// <summary>
        /// Обновляет содержимое контроллов.
        /// </summary>
        private void RefreshControlsContent()
	    {
            //Узнаем формат времени (24 часовой или 12 часовой).
            var timePattern = DateFormat.Is24HourFormat(Application.Context) ? "HH:mm" : "hh:mm tt";

            //Подставляем значения контроллам.
            _alarmSettingsTime.Text = _alarm.DateTime.ToString(timePattern);
            _alarmSettingsDaysOfWeek.Text = _alarm.GetDaysOfWeekString();

            _alarmSettingsLabel.Text = _alarm.Label;
            _alarmSettingsVibrate.Checked = _alarm.IsVibroOn;
            _alarmSettingsTimeIsMoney.Checked = _alarm.IsTimeIsMoneyOn;

            var imageRealPath = GetRealPathToFile(Android.Net.Uri.Parse(_alarm.ImageFileUri));
            _alarmSettingsImage.Text = imageRealPath ?? _alarm.ImageFileUri;

            try
            {
                var soundFilePath = GetRealPathToFile(Android.Net.Uri.Parse(_alarm.SoundFileUri));
                _alarmSettingsSound.Text = soundFilePath;
            }
            catch (Exception)
            {
                _alarmSettingsSound.Text = _alarm.SoundFileUri;
            }

	    }

        /// <summary>
        /// Собирает содержимое контроллов.
        /// </summary>
        private void GetControlsContent()
        {
            //_alarm.ImageFileUri = _alarmSettingsImage.Text;
            _alarm.Label = _alarmSettingsLabel.Text;
            //_alarm.SoundFileUri = _alarmSettingsSound.Text;
            _alarm.IsVibroOn = _alarmSettingsVibrate.Checked;
            _alarm.IsTimeIsMoneyOn = _alarmSettingsTimeIsMoney.Checked;

        }

        /// <summary>
        /// Сохранить будильник
        /// </summary>
		private void SaveAlarm()
		{
            //Если не было выбранного будильника.
            if (_alarmId<0)
		    {
                //Добавляем вновь созданный.
		        GetControlsContent();
                AlarmList.Alarms.Add(_alarm);
                //Включаем его.
                _alarm.IsEnabled = true;
		    }
            else
            {
                GetControlsContent();
                //Иначе меняем существующий
                AlarmList.Alarms[_alarmId].IsEnabled = false;
                AlarmList.Alarms[_alarmId] = _alarm;
                AlarmList.Alarms[_alarmId].IsEnabled = true;
            }
            Finish();
		}



        private void TimePickerCallback (object sender, TimePickerDialog.TimeSetEventArgs e)
        {
            GetControlsContent();
            _alarm.DateTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, e.HourOfDay, 
                e.Minute, 0);
            RefreshControlsContent();
        }

        protected override Dialog OnCreateDialog(int id)
        {
            if (id == TimeDialogId)
                return new TimePickerDialog(this, TimePickerCallback, _alarm.DateTime.Hour, _alarm.DateTime.Minute, 
                    DateFormat.Is24HourFormat(Application.Context));

            return null;
        }

        /// <summary>
        /// При выходе из активити
        /// </summary>
        protected override void OnStop()
        {
            base.OnStop();
            AlarmList.SaveAlarmListInFile();
        }

	}
}

