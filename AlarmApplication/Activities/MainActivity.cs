﻿using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace AlarmApplication.Activities
{
    [Activity(Label = "AlarmApplication", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
    {

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);
			RequestWindowFeature (WindowFeatures.NoTitle);
			// Set our view from the "main" layout resource
			SetContentView(Resource.Layout.MainLayout);
            //AlarmList.DeleteAlarmListDataFile();
            //Загружаем списки будильников.
            AlarmList.LoadAlarmListFromFile();

			var alarmImageButton = FindViewById<ImageButton> (Resource.Id.alarmImageButton);
			var settingsImageButton = FindViewById (Resource.Id.settingsImageButton);
			var timerImageButton = FindViewById (Resource.Id.timerImageButton);
			var exerciseImageButton = FindViewById (Resource.Id.ExerciseImageButton);
            alarmImageButton.Click += (sender, e) => StartActivity(typeof(AlarmListActivity));
            
            settingsImageButton.Click += (sender, e) =>
            {
            };
            
		    exerciseImageButton.Click += (sender, e) =>
		    {
            };
		}

        /// <summary>
        /// При выходе из активити
        /// </summary>
        protected override void OnStop()
        {
            base.OnStop();
            AlarmList.SaveAlarmListInFile();
        }
    }
}

