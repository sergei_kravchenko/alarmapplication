using System;
using System.Collections.Generic;
using System.Reflection;
using System.Resources;
using Android.App;
using Android.Media;
using Android.Runtime;
using Android.Widget;
using Java.Net;
using Java.Util;

namespace AlarmApplication
{
	/// <summary>
	/// Будильник.
	/// </summary>
	[Serializable]
	public class Alarm
	{
	    private bool _isEnabled;

	    /// <summary>
	    /// Дата и время.
	    /// </summary>
	    public DateTime DateTime {get; set;}

	    /// <summary>
	    /// Дни недели для включения.
	    /// </summary>
	    //public Dictionary<DayOfWeek, bool> DaysOfWeek { get; set; }

	    /// <summary>
	    /// Состояние будильника.
	    /// </summary>
	    public bool IsEnabled
	    {
	        get { return _isEnabled; }
	        set 
            {
                var am = new AlarmManagerBroadcastReceiver();

	            if (value)
	            {
                    am.SetAlarm(Application.Context, DateTime, AlarmList.Alarms.IndexOf(this));
                    Toast.MakeText(Application.Context, "Alarm Enabled", ToastLength.Short).Show();
	            }
	            else
	            {
                    am.CancelAlarm(Application.Context, AlarmList.Alarms.IndexOf(this));
                    Toast.MakeText(Application.Context, "Alarm Disabled", ToastLength.Short).Show();
	            }
                _isEnabled = value;
            }
	    }

	    /// <summary>
	    /// Адресс рингтона.
	    /// </summary>
        public string SoundFileUri { get; set; }

        /// <summary>
        /// Адресс картинки.
        /// </summary>
        public string ImageFileUri { get; set; } 

        /// <summary>
        /// Включен ли вибро сигнал.
        /// </summary>
        public bool IsVibroOn { get; set; } 

        /// <summary>
        /// Надпись будильника
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// Включен ли режим время - деньги.
        /// </summary>
        public bool IsTimeIsMoneyOn { get; set; }

	    /// <summary>
		/// Инициализарует дату и время.
		/// </summary>
		public Alarm()
		{
			DateTime = DateTime.Now;
            //DaysOfWeek = new Dictionary<DayOfWeek, bool>
            //{
            //    {DayOfWeek.Monday, true},
            //    {DayOfWeek.Tuesday, true},
            //    {DayOfWeek.Wednesday, true},
            //    {DayOfWeek.Thursday, true},
            //    {DayOfWeek.Friday, true},
            //    {DayOfWeek.Saturday, true},
            //    {DayOfWeek.Sunday, true}
            //};
	        SoundFileUri = RingtoneManager.GetDefaultUri(RingtoneType.Ringtone).ToString();
	        Label = "Label";
            ImageFileUri = "android.resource://AlarmApplication.AlarmApplication/"+Resource.Drawable.AlarmIcon;
	        IsVibroOn = true;
	        IsTimeIsMoneyOn = true;
		}

        /// <summary>
        /// Выдает включенные дни недели.
        /// </summary>
        /// <returns></returns>
	    public string GetDaysOfWeekString()
	    {
            //string daysOfWeek = null;
            //foreach (var dayOfWeek in DaysOfWeek)
            //{
            //    if (!dayOfWeek.Value)
            //    {
            //        foreach (var dayOfWeek2 in DaysOfWeek)
            //        {
            //            if (dayOfWeek2.Value)
            //            {
            //                daysOfWeek += dayOfWeek2.Key + " ";
            //            }
            //        }
            //        return daysOfWeek;
            //    }
            //}
	        return "Every Day";
	    }

	}
}