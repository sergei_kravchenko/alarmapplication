using System;
using System.Collections.Generic;
using System.IO;
using Environment = System.Environment;

namespace AlarmApplication
{
    /// <summary>
    /// ������ �����������
    /// </summary>
	public static class AlarmList
    {
        /// <summary>
        /// ��� ����� ������ ������ �����������.
        /// </summary>
        private const string XMLDataFileName = "AlarmListData.xml";

        /// <summary>
        /// ���� � ����������� �����.
        /// </summary>
        private static readonly string XMLDataFilePath;

        /// <summary>
        /// ������ �����������.
        /// </summary>
        public static List<Alarm> Alarms{ get; set; } 

        /// <summary>
        /// �������������� ����������.
        /// </summary>
		static AlarmList()
		{
			Alarms = new List<Alarm>();

            //�������� ���� � ����������� �����.
            XMLDataFilePath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);

		}

        /// <summary>
        /// ��������� ������ ����������� � XML ����.
        /// </summary>
        public static void SaveAlarmListInFile()
        {
            //��������� � ���� ��� �����.
            var filePath = Path.Combine(XMLDataFilePath, XMLDataFileName);

            var xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(List<Alarm>));
            try
            {
                //��������� ���� �� ������.
                var file = new StreamWriter(filePath, false);
                //������� XML ���� � ������ ������������.
                xmlSerializer.Serialize(file, Alarms);
                //��������� ����.
                file.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// �������� ������ ����������� �� XML �����.
        /// </summary>
        public static void LoadAlarmListFromFile()
        {
            //��������� � ���� ��� �����.
            var filePath = Path.Combine(XMLDataFilePath, XMLDataFileName);
            if (File.Exists(filePath))
            {
                try
                {
                    var xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(List<Alarm>));
                    //��������� ���� �� ������.
                    var file = new StreamReader(filePath);
                    //�������� ������ ����������� �� XML �����.
                    Alarms = (List<Alarm>)xmlSerializer.Deserialize(file);
                    //��������� ����.
                    file.Close();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public static void DeleteAlarmListDataFile()
        {
            var filePath = Path.Combine(XMLDataFilePath, XMLDataFileName);
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
        }
	}
}