﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlarmApplication.Activities;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AlarmApplication
{
    class AlarmListAdapter:SimpleAdapter
    {
        private readonly Activity _activity;
        private readonly IList<IDictionary<string, object>> _alarmList;
        private readonly int _resource;
        private readonly string[] _from;
        private int[] _to;


        protected AlarmListAdapter(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public AlarmListAdapter(Activity context, IList<IDictionary<string, object>> data, int resource, string[] @from, int[] to) : base(context, data, resource, @from, to)
        {
            _activity = context;
            _alarmList = data;
            _resource = resource;
            _from = @from;
            _to = to;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = convertView; // re-use an existing view, if one is supplied
            if (view == null) // otherwise create a new one
                view = LayoutInflater.FromContext(_activity).Inflate(_resource, null);


            // set view properties to reflect data for the given row
            var listItemTime = view.FindViewById<TextView>(Resource.Id.ListItemTime);
            var listItemDaysOfWeek = view.FindViewById<TextView>(Resource.Id.ListItemDaysOfWeek);
            var listItemIsEnabled = view.FindViewById<Switch>(Resource.Id.ListItemIsEnabled);
            var listItemRemoveButton = view.FindViewById<ImageButton>(Resource.Id.ListItemRemoveButton);

            listItemTime.Text = (string) _alarmList[position][_from[0]];
            listItemDaysOfWeek.Text = (string) _alarmList[position][_from[1]];
            listItemIsEnabled.Checked = (bool)_alarmList[position][_from[2]];

            //Подписываемся на события.
            listItemIsEnabled.CheckedChange += (sender, e) =>
            {
                if (AlarmList.Alarms[position].IsEnabled != listItemIsEnabled.Checked)
                {
                    AlarmList.Alarms[position].IsEnabled = listItemIsEnabled.Checked;
                }
            };

            listItemRemoveButton.Click += (sender, args) => RemoveButtonClicked(position);

            // return the view, populated with data, for display
            return view;
        }


        /// <summary>
        /// Вызывает диалог с подтверждением и удаляет будильник. 
        /// </summary>
        /// <param name="position">Номер элемента в списке.</param>
        private void RemoveButtonClicked(int position)
        {
            _activity.RunOnUiThread(() =>
            {
                var alertDialog = new AlertDialog.Builder(_activity);
                alertDialog.SetMessage("This Alarm will be deleted."); // сообщение
                alertDialog.SetPositiveButton("Ok",
                    (o, args) =>
                    {
                        AlarmList.Alarms[position].IsEnabled = false;
                        AlarmList.Alarms.RemoveAt(position);
                        _activity.StartActivity(typeof (AlarmListActivity));
                    }
                    );
                alertDialog.SetNegativeButton("Cancel", (o, args) => { });
                alertDialog.Create();
                try
                {
                    alertDialog.Show();

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            });
        }

    }
}
