using System;
using System.Text;
using AlarmApplication.Activities;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Java.Text;
using Java.Util;

namespace AlarmApplication
{
    [BroadcastReceiver]
	public class AlarmManagerBroadcastReceiver : BroadcastReceiver
    {
        private const string AlarmIdExtra = "alarmId";
        private int _alarmId;

        public override void OnReceive(Context context, Intent intent)
	    {
			StaticWakeLock.LockOn(context);

            if (intent.HasExtra(AlarmIdExtra))
            {
                _alarmId = intent.Extras.GetInt(AlarmIdExtra);
            }
			var scheduledIntent = new Intent(context, typeof(AlarmFiringActivity));
            if (_alarmId >= 0)
            {
                scheduledIntent.PutExtra(AlarmIdExtra, _alarmId);
            }
            scheduledIntent.AddFlags(ActivityFlags.NewTask);
			context.StartActivity(scheduledIntent);

        }


        /// <summary>
        /// ������������� ���������.
        /// </summary>
        /// <param name="context">Context.</param>
        /// <param name="dateTime">���� � �����.</param>
        /// <param name="alarmId">����� ����������.</param>
        public void SetAlarm(Context context, DateTime dateTime, int alarmId)
		{

			var am=(AlarmManager)context.GetSystemService(Context.AlarmService);
			var intent = new Intent(context, Class);
            intent.PutExtra(AlarmIdExtra, alarmId);
            var pi = PendingIntent.GetBroadcast(context, alarmId, intent, PendingIntentFlags.UpdateCurrent);

            var dateTimeNow = DateTime.Now;

            
			var calendar = Calendar.GetInstance(Java.Util.TimeZone.Default);
            if (dateTime.TimeOfDay < dateTimeNow.TimeOfDay)
            {
                calendar.Set(CalendarField.DayOfMonth, dateTimeNow.AddDays(1).Day);
            }
            else
            {
                calendar.Set(CalendarField.DayOfMonth, dateTimeNow.Day);
            }
            //TODO:Delete this
            //calendar.Set(CalendarField.DayOfMonth, dateTimeNow.Day);

            calendar.Set(CalendarField.HourOfDay, dateTime.Hour);
			calendar.Set(CalendarField.Minute, dateTime.Minute);
			calendar.Set(CalendarField.Second, dateTime.Second);
			//������������� ������������� ��������� �� ������ ����
			am.SetRepeating(AlarmType.RtcWakeup, calendar.TimeInMillis, AlarmManager.IntervalDay , pi);
            //am.SetRepeating(AlarmType.RtcWakeup, calendar.TimeInMillis, 1000*10, pi);

		}

        public void SetSnoozeAlarm(Context context, DateTime dateTime, int alarmId)
        {
            var am = (AlarmManager)context.GetSystemService(Context.AlarmService);
            var intent = new Intent(context, Class);
            intent.PutExtra(AlarmIdExtra, alarmId);
            var pi = PendingIntent.GetBroadcast(context, 99999, intent, PendingIntentFlags.UpdateCurrent);


            var calendar = Calendar.GetInstance(Java.Util.TimeZone.Default);
            calendar.Set(CalendarField.HourOfDay, dateTime.Hour);
            calendar.Set(CalendarField.Minute, dateTime.Minute);
            calendar.Set(CalendarField.Second, dateTime.Second);
            //������������� ������������� ��������� �� ������ ����
            am.Set(AlarmType.RtcWakeup, calendar.TimeInMillis, pi);

        }

        private DateTime SetDateTimeInFuture(DateTime value)
        {
            var dateTimeNow = DateTime.Now;
            //���� ��������������� �������� � �������, �� ������ ����� �� ������.
            if ((value.Day <= dateTimeNow.Day) && (value.TimeOfDay < dateTimeNow.TimeOfDay))
            {
                return new DateTime(dateTimeNow.Year, dateTimeNow.Month, dateTimeNow.Day + 1, value.Hour,
                    value.Minute, value.Second);
            }
            return new DateTime(dateTimeNow.Year, dateTimeNow.Month, dateTimeNow.Day, value.Hour,
                value.Minute, value.Second);
        }


        public void CancelAlarm(Context context, int alarmId)
		{
			var intent = new Intent(context, Class);
            var sender = PendingIntent.GetBroadcast(context, alarmId, intent, PendingIntentFlags.UpdateCurrent);
			var alarmManager = (AlarmManager) context.GetSystemService(Context.AlarmService);
			alarmManager.Cancel(sender);
		}

	}
}

