﻿using System;
using Android.App;
using Android.Graphics;

namespace AlarmApplication
{
    internal class ImageResizer
    {
        private static int CalculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight)
        {
            // Raw height and width of image
            var height = (float)options.OutHeight;
            var width = (float)options.OutWidth;
            var inSampleSize = 1D;

            if (height > reqHeight || width > reqWidth)
            {
                inSampleSize = width > height
                                    ? height / reqHeight
                                    : width / reqWidth;
            }

            return (int)inSampleSize;
        }

        public static Bitmap DecodeSampledBitmapFromFile(string filePath, int reqWidth, int reqHeight)
        {
            // First decode with inJustDecodeBounds=true to check dimensions
            var options = new BitmapFactory.Options { InJustDecodeBounds = true };

            var inputSteam = Application.Context.ContentResolver.OpenInputStream(Android.Net.Uri.Parse(filePath));
            using (BitmapFactory.DecodeStream(inputSteam, null, options))
            {
            }

            inputSteam.Close();
            // Calculate inSampleSize
            options.InSampleSize = CalculateInSampleSize(options, reqWidth, reqHeight);

            inputSteam = Application.Context.ContentResolver.OpenInputStream(Android.Net.Uri.Parse(filePath));
            // Decode bitmap with inSampleSize set
            options.InJustDecodeBounds = false;
            var returnBitmap = BitmapFactory.DecodeStream(inputSteam, null, options);
            inputSteam.Close();
            return returnBitmap;
        }


    }
}
