using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace AlarmApplication
{
    /// <summary>
    /// ���������� �������� ��������.
    /// </summary>
    class SquareLinearLayout: LinearLayout
    {
        protected SquareLinearLayout(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public SquareLinearLayout(Context context) : base(context)
        {
        }

        public SquareLinearLayout(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public SquareLinearLayout(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle)
        {
        }

        protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec)
        {

            var width = MeasureSpec.GetSize(widthMeasureSpec);
            base.OnMeasure(MeasureSpec.MakeMeasureSpec(width, MeasureSpecMode.Exactly),
                MeasureSpec.MakeMeasureSpec(width, MeasureSpecMode.Exactly));

        }
    }
}