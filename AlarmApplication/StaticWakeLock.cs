using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AlarmApplication
{
	public class StaticWakeLock
	{

		private static PowerManager.WakeLock _wakeLock = null;

		public static void LockOn(Context context) {
			PowerManager pm = (PowerManager) context.GetSystemService(Context.PowerService);
			//Object flags;
			if (_wakeLock == null)
				_wakeLock = pm.NewWakeLock( WakeLockFlags.Full | WakeLockFlags.AcquireCausesWakeup, "ALARM");
			_wakeLock.Acquire();
		}

		/// <summary>
		/// Ра
		/// </summary>
		/// <param name="context">Context.</param>
		public static void LockOff(Context context) {
			try {
				if (_wakeLock != null)
					_wakeLock.Release();
			} catch (Exception e) {
				throw(e);
			}
		}

	}
}

